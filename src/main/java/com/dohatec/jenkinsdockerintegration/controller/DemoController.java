package com.dohatec.jenkinsdockerintegration.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class DemoController {


    //demo
    @GetMapping
    public String home() {
        return "jenkins-docker";
    }

    //demo
    @GetMapping("/info")
    public String info() {
        return "jenkins ci-cd";
    }

}
